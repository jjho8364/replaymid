package com.innovalic.replaymid.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovalic.replaymid.R;
import com.innovalic.replaymid.adapter.ListViewAdapter2;
import com.innovalic.replaymid.model.ListViewItem2;
import com.innovalic.replaymid.utils.GetChinaListArr;
import com.innovalic.replaymid.utils.GetDaedListArr;
import com.innovalic.replaymid.utils.GetEnglandListArr;
import com.innovalic.replaymid.utils.GetJapanListArr;
import com.innovalic.replaymid.utils.GetMidListArr;
import com.innovalic.replaymid.utils.GetMidListArr2;
import com.innovalic.replaymid.utils.GetMidListArr3;

import java.util.ArrayList;
import java.util.List;

public class MidListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private final String TAG = " MidListActivity -  ";
    String title = "";
    String type = "";
    ListView listView2;
    ArrayList<ListViewItem2> listArr;

    String[] sfArr = new String[5];

    String packageName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mid_list);

        AdView mAdViewListUpper = (AdView) findViewById(R.id.adView_list_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewListUpper.loadAd(adRequest);

        Intent intent = getIntent();
        title = (String)intent.getSerializableExtra("title");
        //imgUrl = (String)intent.getSerializableExtra("imgUrl");
        type = (String)intent.getSerializableExtra("type");

        Log.d(TAG, "title1 : " + title);
        Log.d(TAG, "type1 : " + type);

        //ImageView imageView = (ImageView)findViewById(R.id.listview2_img);
        //Picasso.with(MidListActivity.this).load(imgUrl).into(imageView);

        listArr = new ArrayList<ListViewItem2>();
        classifyTitle(type);

        listView2 = (ListView)findViewById(R.id.listview2);
        listView2.setAdapter(new ListViewAdapter2(MidListActivity.this, getLayoutInflater(), listArr));
        listView2.setOnItemClickListener(this);

        PackageManager pm = getPackageManager();
        List<ApplicationInfo> list = pm.getInstalledApplications(0);
        for (ApplicationInfo applicationInfo : list) {
            String pName = applicationInfo.packageName;   // 앱 패키지
            if(pName.equals("com.mxtech.videoplayer.pro")){
                packageName = "com.mxtech.videoplayer.pro";
                Log.d(TAG, "pacakge : pro ");
                break;
            } else if(pName.equals("com.mxtech.videoplayer.ad")){
                packageName = "com.mxtech.videoplayer.ad";
                Log.d(TAG, "pacakge : ad ");
                break;
            }
        }

        if(packageName.equals("")){
            Toast.makeText(MidListActivity.this, "mx player를 설치하세요. 동영상 떨림 현상이 없어지고, 화질이 향상 됩니다.", Toast.LENGTH_SHORT).show();
        }

        getSfPref();
        //pushSfArr();
    }

    public void pushSfArr(){
        sfArr[4] = sfArr[3];
        sfArr[3] = sfArr[2];
        sfArr[2] = sfArr[1];
        sfArr[1] = sfArr[0];
    }

    public void setSfArr(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("one", sfArr[0]);
        editor.putString("two", sfArr[1]);
        editor.putString("three", sfArr[2]);
        editor.putString("four", sfArr[3]);
        editor.putString("five", sfArr[4]);
        editor.commit();

    }

    public void getSfPref(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        sfArr[0] = sf.getString("one", "");
        sfArr[1] = sf.getString("two", "");
        sfArr[2] = sf.getString("three", "");
        sfArr[3] = sf.getString("four", "");
        sfArr[4] = sf.getString("five", "");
    }

    public void classifyTitle(String type){
        Log.d(TAG, title);
        GetMidListArr getMidListArr = new GetMidListArr();
        GetJapanListArr getJapanListArr = new GetJapanListArr();
        GetEnglandListArr getEnglandListArr = new GetEnglandListArr();
        GetChinaListArr getChinaListArr = new GetChinaListArr();
        GetDaedListArr getDaedListArr = new GetDaedListArr();
        switch (type){
            case "mid" :
                listArr = getMidListArr.getListArr(title);
                if(listArr.size() == 0){
                    GetMidListArr2 getMidListArr2 = new GetMidListArr2();
                    listArr = getMidListArr2.getListArr(title);
                }
                if(listArr.size() == 0){
                    GetMidListArr3 getMidListArr3 = new GetMidListArr3();
                    listArr = getMidListArr3.getListArr(title);
                }
                break;
            case "ild" :
                listArr = getJapanListArr.getListArr(title);
                break;
            case "youngd" :
                listArr = getEnglandListArr.getListArr(title);
                break;
            case "joongd" :
                listArr = getChinaListArr.getListArr(title);
                break;
            case "daed" :
                listArr = getDaedListArr.getListArr(title);
                break;
            default:
                Log.d(TAG, "come in default");
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "title : " + listArr.get(position).getTitle());
        pushSfArr();
        sfArr[0] = listArr.get(position).getTitle() + " - " + type;
        setSfArr();

        /*Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
        //Intent intent = new Intent(MidListActivity.this, PlayerActivity.class);
        intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
        //Log.d(TAG, "finalUrl : " + listArr.get(position).getVideoUrl());
        startActivity(intent);*/
        if(listArr.get(position).getVideoUrl().contains("mgapi.wecandeo.com") || listArr.get(position).getVideoUrl().contains("videofarm.daum") || listArr.get(position).getVideoUrl().contains("d1wst0behutosd") || listArr.get(position).getVideoUrl().contains("serviceapi") || listArr.get(position).getVideoUrl().contains("blog.naver.com")){
            Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
            intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
            startActivity(intent);
        } else {
            // 패키지 검사후 mx 플레이어가 있으면
            if(packageName.equals("")){
                Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
                intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
                startActivity(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri videoUri = Uri.parse(listArr.get(position).getVideoUrl());
                intent.setDataAndType( videoUri, "application/x-mpegURL" );
                intent.putExtra("decode_mode", (byte)2);
                intent.putExtra("video_zoom", 0);
                intent.setPackage(packageName);
                startActivity(intent);
            }
        }
    }
}
