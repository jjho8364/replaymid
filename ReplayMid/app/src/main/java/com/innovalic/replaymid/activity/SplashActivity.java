package com.innovalic.replaymid.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.innovalic.replaymid.R;

public class SplashActivity extends AppCompatActivity {

    private InterstitialAd interstitialAd2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        AdRequest adRequest = new AdRequest.Builder().build();

        interstitialAd2 = new InterstitialAd(this);
        interstitialAd2.setAdUnitId("ca-app-pub-9440374750128282/4536287252");
        interstitialAd2.loadAd(adRequest);

        initialize();

    }

    private void initialize() {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                finish();    // 액티비티 종료
            }
        };

        handler.sendEmptyMessageDelayed(0, 1000);    // ms, 3초후 종료시킴
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd2.isLoaded()){
            interstitialAd2.show();
        }
    }
}
