package com.innovalic.replaymid.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;

import com.innovalic.replaymid.R;
import com.innovalic.replaymid.activity.MidListActivity;
import com.innovalic.replaymid.adapter.GridViewAdapter;
import com.innovalic.replaymid.adapter.ListViewAdapter;
import com.innovalic.replaymid.model.GridViewItem;
import com.innovalic.replaymid.utils.NetworkUtil;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-11.
 */
public class Fragment02 extends Fragment {
    private String TAG = "Fragment02_TAG ";
    private ArrayList<GridViewItem> gridArr;
    private GridView gridView;

    private EditText editText;
    private GridViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        String model = Build.MODEL.toLowerCase();
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel=Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        //Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //int contactsCnt = c.getCount();
        //if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        //if(false){
        if ( (model.equals("sph-d720") || model.contains("nexus") || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);

            gridArr = new ArrayList<GridViewItem>();
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://cafefiles.naver.net/20160716_67/realart_ha_1468656716747oK1hN_JPEG/Suizokukan_zpsjefbgwru.jpg","수족관 걸"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160722_4/winsroads_1469149728495nHNXu_JPEG/ON_%EC%B6%DF%C8%DB%F3%F1%AA%DE%DB%CE%AF%D4%F6%D3%D1%DD%EF%D2%AF%ED%AD___%E0%A4%AB%C6%AB%EC%AB%D3%DB%AF%E1%EA_%AB%AB%AB%F3%AB%C6%AB%EC.jpeg","ON 이상범죄수사관 토도 히나코"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20140106_114/zosen98_1388989084133ApNSB_JPEG/%C7%CF%B5%E5%B3%CA%C6%AE%BC%F6%C7%D0%BC%D2%B3%E0%B0%A1%C1%C1%BE%C6%C7%CF%B4%C2%BB%E7%B0%C7%BA%CE.jpg","하드너트!수학소녀가 좋아하는 사건부"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://cafefiles.naver.net/20160801_48/jjy1424_1470057068661WAKXy_PNG/_d_1.png","사폐"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160807_253/u_hakseang_1470579146233qyqv4_JPEG/%C0%CF%B5%E5_%BD%C3%B0%A3%C0%BB_%B4%DE%B8%AE%B4%C2_%BC%D2%B3%E0__%282%29.JPG","시간을 달리는 소녀"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160720_60/gkgmd4122_1468996538197DmJ6P_JPEG/0-1.jpg","호프~기대제로의 신입사원"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20151211_295/catiee_1449836840079zQky5_JPEG/maxresdefault.jpg","옆자리 세키군"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://cafefiles.naver.net/20160720_54/realart_ha_1469018400956MHqrx_JPEG/Tokuyama_Daigoro_wo_Dare_ga_Koroshitaka.jpg","도쿠야마 다이고로를 누가 죽였나"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160723_246/ideamode_1469212970272mUE1H_JPEG/11.jpg","그리고 아무도 없었다"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20140507_285/catiee_1399432589556xi00f_JPEG/tokai-tv_com_20140504_155202.jpg","사쿠라신쥬"));
            //gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/206/206903.jpg","장난스런 키스 Love in Tokyo 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20150321_156/mido22kr_14269373989052Lul7_PNG/VideoScreenshot_20141205-235607.png","장난스런 키스 Love in Tokyo 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160520_264/narye1126_146370393893674DJ0_JPEG/BPUA0_z2zxB_JxeUj_qMztp.jpg","세상에서 가장 어려운 사랑"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/156/156300.jpg","호타루의 빛 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/156/156305.jpg","호타루의 빛 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/159/159318.jpg","프로포즈 대작전"));
            gridArr.add(new GridViewItem("http://kinimage.naver.net/20160330_207/1459343895005AycJt_JPEG/1459343894831.jpg?type=w620","전개걸"));
            gridArr.add(new GridViewItem("http://postfiles12.naver.net/20150302_267/pqdpyiuvs_1425275450114e8fi9_JPEG/0%BB%E7%C1%F81.jpg?type=w2","아네고"));
            gridArr.add(new GridViewItem("http://postfiles6.naver.net/20150204_261/japansisa_1423035242376iYq5q_JPEG/2.jpg?type=w1","사프리"));
            gridArr.add(new GridViewItem("http://postfiles9.naver.net/20151105_216/cyanzzang_1446714861928jzjJ5_PNG/01.png?type=w3","스타맨 : 이 별의 사랑"));
            gridArr.add(new GridViewItem("http://postfiles3.naver.net/20160517_210/aprileehj_14634929138334gK3H_JPEG/2016-05-17_22%3B48%3B16.jpg?type=w1","오늘은 회사 쉬겠습니다"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160712_91/film1982_1468257160983kRDbs_JPEG/%C0%CF%B5%E5_%B8%F1%BC%D2%B8%AE%C0%C7%BB%E7%B6%FB_1%C8%AD.jpg","목소리 사랑"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/48/16/57_664816_poster_image_1462863934144.jpg","사채꾼 우시지마 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/69/95/86/57_1699586_poster_image_1462864259232.jpg","사채꾼 우시지마 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/56/37/95/57_3563795_poster_image_1462861004641.jpg","사채꾼 우시지마 시즌3"));
            //gridArr.add(new GridViewItem("http://postfiles7.naver.net/20150204_198/japansisa_1423035887093v9hrE_PNG/5.png?type=w1","라스트 신데렐라"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/44/18/18/57_2441818_poster_image_1437028133288.jpg","마더게임 : 그녀들의 계급"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=f120x171&quality=10&q=http://movie.phinf.naver.net/20111223_135/1324622932462Vmpg4_JPEG/movie_image.jpg?type=w640_2","노다메 칸타빌레"));
            gridArr.add(new GridViewItem("http://postfiles16.naver.net/20151118_63/funkstyle_1447852466859ueekz_JPEG/2015-11-18_22%3B13%3B53.jpg?type=w2","쿠로사키군의 말대로는 되지 않아"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20150917_245/xoaprilxo_14424266040742AcXL_JPEG/2015-09-17_03%3B02%3B29.JPEG","문제 있는 레스토랑"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20130714_20/sohyun890_1373778252693420zX_JPEG/%B0%ED%B5%B6%C7%D1%B9%CC%BD%C4%B0%A1%BD%C3%C1%F03.jpg","고독한 미식가 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/95/84/90/57_1958490_poster_image_1404783117831.jpg","고독한 미식가 시즌4"));


            //gridArr.add(new GridViewItem("","데스노트"));

            gridArr.add(new GridViewItem("http://postfiles2.naver.net/20160611_129/amahime_1465648650053svY2Y_PNG/%BB%E7%BF%EB%C0%DA_%C1%F6%C1%A4_1.png?type=w2","쓰르라미 울 적에"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/62/91/81/57_2629181_poster_image_1430384636093.jpg","데스노트"));

            editText = (EditText)view.findViewById(R.id.anilist_edit);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                }
            });

            gridView = (GridView)view.findViewById(R.id.gridview);
            adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), MidListActivity.class);
                    intent.putExtra("title", gridArr.get(position).getTitle());
                    intent.putExtra("type", "ild");
                    startActivity(intent);
                }
            });
        }

        return view;

    }

}
