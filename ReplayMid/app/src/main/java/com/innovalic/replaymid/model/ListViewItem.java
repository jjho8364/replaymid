package com.innovalic.replaymid.model;

/**
 * Created by Administrator on 2016-07-06.
 */
public class ListViewItem {

    //String listUrl;
    String imgUrl;
    String title;

    public ListViewItem(String imgUrl, String title) {
        //this.listUrl = listUrl;
        this.imgUrl = imgUrl;
        this.title = title;
    }

    /*public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }*/

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
