package com.innovalic.replaymid.utils;

import android.util.Log;

import com.innovalic.replaymid.model.ListViewItem2;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-31.
 */
public class GetDaedListArr {
    private String TAG = "GetDaedListArr - ";
    ArrayList<ListViewItem2> listArr;

    public ArrayList<ListViewItem2> getListArr(String title) {
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();
        Log.d(TAG, "title : " + title);
        switch (title) {
            case "닥터후 시즌1" :
                listArr.add(new ListViewItem2("닥터후 시즌1 - 01화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20101102062052680prfshv0xj7a1a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 02화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/70/201011020627104155sth4krtt8k74.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 03화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201011020627330718rlbjzxwyxv30.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 04화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/96/2010110206275914977os5gjx0ftm3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 05화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/05/20101102062847946jkuoj3evh6pzl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 06화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/02/20101102062052712s4gmaxscsoxl7.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 07화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/03/20101102062153712pg5rm1je5i355.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/09/20101102062639774j628x3xwojfyc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 09화", "http://183.110.26.41/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/13/20101102062609399ismf2gsa9k4zp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 10화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/14/20101102062820165fsghcoekv8gc9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 11화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/20/20101102062819212zvcxd9gc59c17.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 12화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/22/201011020628214627ucusbw6jggmd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 13화 완결", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101102062821165qvct2l9cbi0om.flv"));
                break;
        }


        return listArr;
    }
}
