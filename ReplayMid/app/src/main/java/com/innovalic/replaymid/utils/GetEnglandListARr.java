package com.innovalic.replaymid.utils;

import android.util.Log;

import com.innovalic.replaymid.model.ListViewItem2;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-12.
 */
public class GetEnglandListArr {
    private String TAG = "GetEnglandListArr - ";
    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title) {
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();
        Log.d(TAG, "title : " + title);
        switch (title) {
            case "닥터후 시즌1" :
                listArr.add(new ListViewItem2("닥터후 시즌1 - 01화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20101102062052680prfshv0xj7a1a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 02화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/70/201011020627104155sth4krtt8k74.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 03화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201011020627330718rlbjzxwyxv30.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 04화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/96/2010110206275914977os5gjx0ftm3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 05화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/05/20101102062847946jkuoj3evh6pzl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 06화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/02/20101102062052712s4gmaxscsoxl7.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 07화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/03/20101102062153712pg5rm1je5i355.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/09/20101102062639774j628x3xwojfyc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 09화", "http://183.110.26.41/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/13/20101102062609399ismf2gsa9k4zp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 10화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/14/20101102062820165fsghcoekv8gc9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 11화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/20/20101102062819212zvcxd9gc59c17.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 12화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/22/201011020628214627ucusbw6jggmd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 13화 완결", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101102062821165qvct2l9cbi0om.flv"));
                break;
            case "닥터후 시즌2" :
                listArr.add(new ListViewItem2("닥터후 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=2Sq8JbsnzQI$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=9XoCsXkR508$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 03화", "http://183.110.26.90/redirect/cmvs.mgoon.com/storage2/m2_video/2010/08/31/4091263.mp4?px-bps=1385829&px-bufahead=10"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ZDLbDSIYoVg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wcfbGqBXWUA$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=0XeFNvG2s6c$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=51mTbbPiSK8$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=u59WN-VM9Sg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Zu1XSDK0qFc$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=QHbBhXKVd9A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=upbzVrA2dJk$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=74EWwOaaO0A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=zwIpR7dAd7g$"));
                break;
            case "닥터후 시즌3" :
                listArr.add(new ListViewItem2("닥터후 시즌3 - 01화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/48/201011010326395240mjm5qroezf3j.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 02화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/60/20101101032616509622osc0g1bs6a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 03화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/71/20101101032617368s591udn5n1zi0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 04화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/90/20101101032627290lavqi4nmfvqlc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 05화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/30/20101028094042059lvglsref8gcbd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 06화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/42/20101029054303371ry1vormdpmtpi.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/54/20101029054241871rww2isbyx1yqr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/20100926064207251vmzxfx8dkxv5w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 09화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/61/201009260649134549q1bd7setgtgy.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 10화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/201010290554380436067z5v4ds30w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 11화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/81/20101029055458871idpvouyrzj4ro.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 12화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/00/20101029055441340gjlqgmtjq4yli.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 13화 완결", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101029055442121ixr0pg9u4dcpt.flv"));
                break;
            case "닥터후 시즌4" :
                listArr.add(new ListViewItem2("닥터후 시즌4 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/2010080218202800960u4kge1j1zjl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/89/20101029024502559o7mfcy9kdnqu0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 03화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/12/20101029023649902we40xzi2cb6sk.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 04화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/69/201010280810091990v1cpawxy53oq.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 05화", "http://183.110.26.75/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/06/20101028075358496vlv0dh6ui34eg.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 06화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/51/201310032059365185zz0ia2dgysh5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 07화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/42/20101028052027527r7g76gn76d3d4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 08화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/60/201010280514077319b6mfwwmgpa3h.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 09화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/85/201010280514107461yv1zqjopvufz.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 10화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/26/20101027093734418axsf5g315qeuf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 11화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/11/20101027092320762p5yy8m30luwmr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 12화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/26/20101027080600231gdmi9318ve6aw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 13화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/01/20101027062633387a68rbg7mbtfr3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 14화 완결", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/06/20101027062141981pfd6yybg6l81b.flv"));
                break;
            case "닥터후 시즌5" :
                listArr.add(new ListViewItem2("닥터후 시즌5 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/57/2010091205592524001cnnxgm6kv4n.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/04/20100913054720537g19stf5xzztah.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/29/201007280831388542c5xx77lnlyg5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/37/20100904073508064rvt4wdlgyp9n4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/17/20100907093504587ap4h8msvmbxkr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/25/20100616054111525orskvuobvv1cf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/59/20100522114114000qkr4wpmjaqa01.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/15/20100529205355468vbyfn0ai2m5wm.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/20101219100328507imdbqcm3h43dp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/89/20101219104835179xnt13ep7qi57b.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/37/20101219105704648vvvvst8fv9aub.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/53/20101219110455663c25bme90wjke2.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 13화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/11/201012191128238207wq6wp80b6y3w.flv"));
                break;
            case "닥터후 시즌6" :
                listArr.add(new ListViewItem2("닥터후 시즌6 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/79/20121101010506925382ctzv77n1iy.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201211010115199565heu5fpk7tid2.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/81/20121101012107534zxfjnka07tmi9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/16/20121101013012909zy7cr6o6k0wog.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/17/20121101013030175ve51zpgaspuxg.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/18/20121101013032925tdc07elhbkdnv.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/51/20121101083352768y9mtryl4xc4mu.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20121101083353518480hiriqhbfy8.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/53/20121101083354065l3nq6vglh6ja1.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/54/20121101083354471o3o67kz5ufevf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/62/201211010922203467to49t3v3xrib.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/63/2012110109222083153dppg8berbgc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 13화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/64/20121101092221331joy3ubesvs3a0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 크리스마스 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/60/20130224213418040lhsbx7dzqkcng.flv"));
                break;
            case "닥터후 시즌7" :
                listArr.add(new ListViewItem2("닥터후 시즌7 - 01화", "http://drama.cd/files/videos/2015/04/24/1429863602b8741-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 02화", "http://drama.cd/files/videos/2015/04/24/1429863677e6f59-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 03화", "http://drama.cd/files/videos/2015/04/24/14298637506b716-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 04화", "http://drama.cd/files/videos/2015/04/24/1429863860bfcc8-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 05화", "http://drama.cd/files/videos/2015/04/24/1429863927d0ea0-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 06화", "http://drama.cd/files/videos/2015/04/24/14298641681bc6e-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 07화", "http://drama.cd/files/videos/2015/04/24/142986422541bb8-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 08화", "http://drama.cd/files/videos/2015/04/24/14298643394cc9c-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 09화", "http://drama.cd/files/videos/2015/04/24/14298644274cc17-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 10화", "http://drama.cd/files/videos/2015/04/24/1429864538f2f1d-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 11화", "http://drama.cd/files/videos/2015/04/24/142986462571a96-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 12화", "http://drama.cd/files/videos/2015/04/24/1429864692e0c3b-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/33/20130520153521155e7tze4mdpargw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 50주년 기념 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/74/20131125195901638pkdrb9niv9bb3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 더 파이브 이쉬 닥터 리뷰", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/43/20131205170523804h20545hbhxrlo.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 크리스마스 스페셜", "http://drama.cd/files/videos/2015/04/24/1429864896e6b6b-sd.mp4"));
                break;
            case "닥터후 시즌8" :
                listArr.add(new ListViewItem2("닥터후 시즌8 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/28/20140826055210171jidc5wguygrxa.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/41/20140901140949093mxfm1ech6noft.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/96/201409081630399531o4fazsw5hcwl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 04화", "http://drama.cd/files/videos/2015/04/25/142992907281f1f-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/77/201409212048548595vx0ulz0cx3dr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 06화", "http://drama.cd/files/videos/2015/04/25/1429929345001ec-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/27/20150114093727466wkqrbk0dwyaz0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 08화", "http://drama.cd/files/videos/2015/04/25/1429929643c37e1-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/02/20141020070342640l8zm6d8smkys4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/27/20141027172813156zt9x6hy8ja7l4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/86/201411021844548902npwmoeab6u7m.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/06/20141109163646298gt5x308ctltnw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 크리스마스 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/49/20141226181536914wm6cyrujii2ij.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 50주년 다큐드라마", "ttp://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/79/20131125175456531feasi0d5cblfb.flv"));
                break;
            case "닥터후 시즌9" :
                listArr.add(new ListViewItem2("닥터후 시즌9 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/201509300624466922b54tbgbz2m2b.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/201510011905390689t6lhxwy7bg3p.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/20151007070324063eqd04vqlm4rde.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/23/20151011131440128ecg89veohnawu.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/02/20151019061046349aslfjmm22yvjs.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/61/20151025171348116aq1tvmhg0t32i.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/66/20151102061033566t058njdbltisf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151110212455179wbojkkbfv66ss.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20151123061709623ebv1hrbz6juct.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/20151123061948349txxd71u3fgq43.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151130065212189omdtriwotfjfx.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/20151207060229328jczji0n06zzng.flv"));
                listArr.add(new ListViewItem2("닥터후 크리스마스 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20151227122640516weyx8p6hrojt0.flv"));
                break;
            case "하우스 오브 카드 시즌1" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 01화", "http://drama.cd/files/videos/2015/04/24/142983730032ae7-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 02화", "http://drama.cd/files/videos/2015/04/24/1429837428f5fb5-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 03화", "http://drama.cd/files/videos/2015/04/24/14298375356a681-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 04화", "http://drama.cd/files/videos/2015/04/24/142983765624199-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 05화", "http://drama.cd/files/videos/2015/04/24/1429837751f2eee-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 06화", "http://drama.cd/files/videos/2015/04/24/14298378391e26b-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 07화", "http://drama.cd/files/videos/2015/04/24/142983795296f3d-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 08화", "http://drama.cd/files/videos/2015/04/24/142983803708f09-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 09화", "http://drama.cd/files/videos/2015/04/24/1429838139cad56-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 10화", "http://drama.cd/files/videos/2015/04/24/1429838231a14fc-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 11화", "http://drama.cd/files/videos/2015/04/24/14298383323f4bb-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 12화", "http://drama.cd/files/videos/2015/04/24/14298384199b94a-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 13화 완결", "http://drama.cd/files/videos/2015/04/24/1429838550df653-sd.mp4"));
                break;
            case "하우스 오브 카드 시즌2" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 01화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20140217163402787g90i1waltzhyr.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 02화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20140221151728927zepf6bdtg3msp.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 03화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/49/201402231624337546uf63wusd1z0q.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 04화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20140224234631203jebw6s6ivd6vq.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 05화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20140225161625972sznqwyn9cvy1d.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 06화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/64/20140226160913597huufts3bw7lug.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 07화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/83/20140227100843988n05emjbn4l4c2.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 08화", "http://14.0.102.159/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/69/20140228181056894tu02urz49ax64.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 09화", "http://14.0.103.157/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/59/20140301115333722yczumft62dohj.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 10화", "http://14.0.102.158/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/201403021544278797ywoc4ywlfs08.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 11화", "http://14.0.102.158/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/08/20140304142301769ynlyselh2avb3.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 12화", "http://drama.cd/files/videos/2015/04/24/1429866689394c8-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 13화", "http://14.0.102.150/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/09/20140309142334193bf3pdynrwfzxl.flv"));
                break;
            case "하우스 오브 카드 시즌3" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 01화", "http://drama.cd/files/videos/2015/04/24/14298391859cbce-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 02화", "http://drama.cd/files/videos/2015/04/24/142983932658057-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 03화", "http://drama.cd/files/videos/2015/04/24/1429839539a402a-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 04화", "http://drama.cd/files/videos/2015/04/24/14298396579cf40-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 05화", "http://drama.cd/files/videos/2015/04/24/142983984522f3a-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 06화", "http://drama.cd/files/videos/2015/04/24/142984002364f2a-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 07화", "http://drama.cd/files/videos/2015/04/24/1429840153a9145-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 08화", "http://drama.cd/files/videos/2015/04/24/1429840246ddda4-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 09화", "http://drama.cd/files/videos/2015/04/24/1429840361d6a4d-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 10화", "http://drama.cd/files/videos/2015/04/24/142984059573123-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 11화", "http://drama.cd/files/videos/2015/04/24/1429840792f090f-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 12화", "http://drama.cd/files/videos/2015/04/24/14298410120e280-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 13화", "http://drama.cd/files/videos/2015/04/24/14298411195863c-sd.mp4"));
                break;
            case "셜록 시즌1" :
                listArr.add(new ListViewItem2("셜록 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/o/eowjszkem132/73/201206101111491715psxd5yke3fpx.flv"));
                listArr.add(new ListViewItem2("셜록 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/w/l/wldbs1589/12/20120212213831629vrk52ct5bu180.flv"));
                listArr.add(new ListViewItem2("셜록 시즌1 - 03화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/w/l/wldbs1589/23/20120212220935910u0c2ftjkp0t7l.flv"));
                break;
            case "셜록 시즌2" :
                listArr.add(new ListViewItem2("셜록 시즌2 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/37/20120108071011868tfj7ap85e4i15.flv"));
                listArr.add(new ListViewItem2("셜록 시즌2 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/201201141640451439ac0ajnyuo2vf.flv"));
                listArr.add(new ListViewItem2("셜록 시즌2 - 03화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/58/20120118192403072fxeowvz16d8cj.flv"));
                break;
            case "셜록 시즌3" :
                listArr.add(new ListViewItem2("셜록 시즌3 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v95faYY0AbK06pp6HHmEHw7"));
                listArr.add(new ListViewItem2("셜록 시즌3 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/24/20140106223353417t05vhajx88isp.flv"));
                listArr.add(new ListViewItem2("셜록 시즌3 - 03화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/20140114044543442whuwirfuc6aea.flv"));
                break;
            case "마이 매드 팻 다이어리 시즌1" :
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 01화", "http://61.110.249.26/redirect/cmvs.mgoon.com/storage2/m2_video/2013/02/21/5299678.mp4?px-bps=1411593&px-bufahead=10"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2013/02/24/5301378.mp4?px-bps=1397925&px-bufahead=10"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 03화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage4/m2_video/2013/02/28/5304583.mp4?px-bps=1386271&px-bufahead=10"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 04화", "http://14.0.68.248/redirect/cmvs.mgoon.com/storage2/m2_video/2013/03/05/5307392.mp4?px-bps=1396983&px-bufahead=10"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 05화", "http://14.0.68.246/redirect/cmvs.mgoon.com/storage4/m2_video/2013/03/08/5309478.mp4?px-bps=1398249&px-bufahead=10"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌1 - 06화", "http://61.110.249.26/redirect/cmvs.mgoon.com/storage1/m2_video/2013/03/10/5310508.mp4?px-bps=1403536&px-bufahead=10"));
                break;
            case "마이 매드 팻 다이어리 시즌2" :
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4a01MdllFdFdssMvL6YZBY"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 02화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDx7IipjrxOguJhZELjSRYNwdkUzzipGQWVyImewNsKntLgw7CQFPNodwvlvZQnisMpQ9t2RaEaa4Gr&ch=true&type=pseudo"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc19dORFdNFeFCCeZMlCZ8E"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9cb41vByQyBQhh3mzryLyT"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 05화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDx7IipjrxOguJhZELjSRYNzh1iiQoUk6WVgYpHPAUgkEVgw7CQFPNodyuIXqQrDtCEtt2RaEaa4Gr&ch=true&type=pseudo"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v612d6rsfRvRsggRsfuwRVf"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌3 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v33aevYgY0QvMJJVMv10pAF"));
                break;
            case "마이 매드 팻 다이어리 시즌3" :
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌3 - 01화", "https://vid.me/e/alkw?stats=1&tools=1"));
                listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌3 - 02화", "http://www.dailymotion.com/cdn/H264-512x384/video/x2wdwo2.mp4?auth=1470670376-2562-x778s5ee-129cefe896b806a052162e5912287436"));
                //listArr.add(new ListViewItem2("마이 매드 팻 다이어리 시즌3 - 03화", "http://video.nmv.naver.com/blog/blog_2015_07_19_75/a8d194594959ec72b101565d2b909d0cebc5_ugcvideo_480P_01.mp4?key=MjEwMzIxMTA0OTE3NTM0OTIzMTE0MjM2MDE5dmlkZW8ubm12Lm5hdmVyLmNvbTA4Mi9ibG9nL2Jsb2dfMjAxNV8wN18xOV83NS9hOGQxOTQ1OTQ5NTllYzcyYjEwMTU2NWQyYjkwOWQwY2ViYzVfdWdjdmlkZW9fNDgwUF8wMS5tcDQzMTIxMzE1NTAwOHpreG0yMDAxMjI5MzExOU5ITk1WMDAwMDAwNjM3NzA1MTQ4MA==&px-bps=963117&px-bufahead=3&in_out_flag=1"));
                break;
            case "스킨스 시즌1" :
                listArr.add(new ListViewItem2("스킨스 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/91/20101210191426495p4696u1mjcqxh.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20101210191447589ycbs081bs0fxp.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/80/20101210191508042cvp3y9picipc0.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/24/20110123153424521u9cb6c6y2voj5.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/74/201011272222466625oymaswapipel.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/28/201101291522368557v7akh60phlri.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/90/20101227194827486k8jvzuq1hopv9.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/56/2011011301555499676y4f6apj3d05.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌1 - 09화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/a/s/asd8888/31/20100704154102269fljgk259cuheu.flv"));
                break;
            case "스킨스 시즌2" :
                listArr.add(new ListViewItem2("스킨스 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=p-g-Kbsdyco$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=HFPBNcaYBOo$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Gsr2jsa7sN0$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=cH6XHaPCs_M$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Agsz68Mj4Bk$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=_aPMtMGWLNs$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=nzewngRH1y0$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=4s6TU7cfRcY$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=avcHxxN9Tak$"));
                listArr.add(new ListViewItem2("스킨스 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=jSxk4nC6K9g$"));
                break;
            case "스킨스 시즌3" :
                listArr.add(new ListViewItem2("스킨스 시즌3 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=1YsU1vC8fLM$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=xS5-pjJsMno$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=bYEJXfrYhyE$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=TQP5hJ63pUA$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=B_ILiIYlBeQ$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=xItytF6Kpuk$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=GfrTWWxOvN8$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=GsiLsZNt0AE$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=HtYc720DLz4$"));
                listArr.add(new ListViewItem2("스킨스 시즌3 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=G3dpe7QTCdo$"));
                break;
            case "스킨스 시즌4" :
                listArr.add(new ListViewItem2("스킨스 시즌4 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/25/2010052806485005930mwsa997bcnu.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/53/201005280702193881ckkosmpgfc1t.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/12/20100528071526825mevrfpsjytak0.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/69/20100528072821950mt0b7o1kgj4uc.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/27/20100528074141091g967ksxke8jcl.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/81/2010052807552143425l8zdzq0mfsa.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/47/20100528080848731zmccrbacd9nci.flv"));
                listArr.add(new ListViewItem2("스킨스 시즌4 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/98/20100528082241731eb32r1vwsr73x.flv"));
                //listArr.add(new ListViewItem2("스킨스 시즌4 - 09화", ""));
                //listArr.add(new ListViewItem2("스킨스 시즌4 - 10화", ""));
                break;
            case "스킨스 시즌5" :
                listArr.add(new ListViewItem2("스킨스 시즌5 - 01화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 02화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 03화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 04화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 05화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 06화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 07화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 08화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 09화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌5 - 10화", ""));
                break;
            case "스킨스 시즌6" :
                listArr.add(new ListViewItem2("스킨스 시즌6 - 01화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 02화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 03화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 04화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 05화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 06화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 07화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 08화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 09화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌6 - 10화", ""));
                break;
            case "스킨스 시즌7" :
                listArr.add(new ListViewItem2("스킨스 시즌7 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2013/07/03/5398979.mp4?px-bps=1380381&px-bufahead=10"));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2013/07/10/5405426.mp4?px-bps=1371186&px-bufahead=10"));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 03화", "http://cmvs.mgoon.com/storage1/m2_video/2013/07/17/5411311.mp4?px-bps=1374063&px-bufahead=10"));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 04화", "http://cmvs.mgoon.com/storage4/m2_video/2013/07/25/5417985.mp4?px-bps=1376368&px-bufahead=10"));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2013/08/01/5424439.mp4?px-bps=1369479&px-bufahead=10"));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 06화", "http://cmvs.mgoon.com/storage4/m2_video/2013/08/06/5428850.mp4?px-bps=1381996&px-bufahead=10"));
                /*listArr.add(new ListViewItem2("스킨스 시즌7 - 07화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 08화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 09화", ""));
                listArr.add(new ListViewItem2("스킨스 시즌7 - 10화", ""));*/
                break;
            case "마법사 멀린 시즌1" :
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v90eeese9Q72aWS7a2jfQjS"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v34dcrcGorxuogCbcoRGuxR"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v0a95T3hp3pTYSuYgOcgYca"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf524NDD97b96NcMRWD2MbY"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vfaa0YKmYKBKm8CIeKHKJSI"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va597ZhO3hO3XcZwkLLkZXk"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v72f54vljjnTj4lk5PmnkvX"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9c83TRiRITrVNRfKTIbxVT"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v52d6TATeNENfNa7SsH6Gaf"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2e1eXflNEXXrlXwgfPwnrr"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v73b9LCeCmCtmctGqmqtcCm"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v99d3gy7g1U1DQyuN1v1odu"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌1 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va05d4GGSrGp2p55SrB2rXI"));
                break;
            case "마법사 멀린 시즌2" :
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9133vMdddabbntTivRoMen"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6365QgOFOYDfQFf3QrKJcO"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v0f8dNzaHaUNjN99vIUC2zT"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v88762FUGB82B8b0GMJFyJ8"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6306bfVibytnVXKrfiddLd"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v3d4buLuiGGrCCJpZihZheC"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6373bZiuD4ZZEbs3aLELm8"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6373bZiuD4ZZEbs3aLELm8"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2876aA7ZPEPZWEvICZ7v9x"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vd0b3twO1Gvtv1UKzKK1K0G"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4bd2aNaZYNay11NUNkDYEP"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vefcdiXgWgIiMSiCRCIwI4F"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5260vvWTo7WO4Obxlg7gCK"));
                break;
            case "마법사 멀린 시즌3" :
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873645.mp4?px-bps=1366488&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 02화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873689.mp4?px-bps=1362148&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873659.mp4?px-bps=1384807&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873683.mp4?px-bps=1362981&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873705.mp4?px-bps=1373217&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 06화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873718.mp4?px-bps=1373076&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 07화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873724.mp4?px-bps=1365258&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 08화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873731.mp4?px-bps=1367722&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 09화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873852.mp4?px-bps=1367040&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 10화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873847.mp4?px-bps=1404409&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 11화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873837.mp4?px-bps=1362280&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 12화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873854.mp4?px-bps=1370517&px-bufahead=10"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌3 - 13화", "http://cmvs.mgoon.com/storage2/m2_video/2012/01/09/4873841.mp4?px-bps=1369125&px-bufahead=10"));
                break;
            case "마법사 멀린 시즌4" :
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/64/20121117124050334gfwqxe6frvnp9.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/81/20121117124716865tyob6y8hjra9o.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/13/20121117125356303qv7rtguew7a1k.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/20121117130023053jb2ysruhkli8p.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/64/20121117130654178p03u9jrrvw215.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/71/20121118171425099o6e8zujw6y8p8.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/45/20121118173237974sus76doj9ienf.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/82/20121118174819584788d2t8lgzkcc.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/13/20121118175559834ie0expfist0hx.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/36/201211181802049286exec70sczpor.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/62/20121118190441740pspgpons5js4y.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/05/201211181912487870twtxat7q2pdb.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌4 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/52/20121118192310803i1ymxexods954.flv"));
                break;
            case "마법사 멀린 시즌5" :
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/54/20121119134216740udbtebqtdlqx0.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/77/20121119134921178ioagfep62lcwm.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/20121119135651584u650jejpf3lm7.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/20/20121119140341959hwllrg1x8kdvm.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/18/201211191422323493m2mhyg5takw0.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/57/20121119145327896l0zk0skg8b7s7.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/87/20121119150015318nex5mhxnv7f3t.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/99/201211260911336253gaauwp3bzqto.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/33/20121204112640475zkrlug0ljgkc8.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/30/201212121114409338s37pvv6w2470.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/36/201212171041255353t1ykgc950tek.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/70/201212232322326890reja9lp7z3oc.flv"));
                listArr.add(new ListViewItem2("마법사 멀린 시즌5 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/70/20121226092419532siet4yyh9ebcv.flv"));
                break;
            case "피키 블라인더스 시즌1" :
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=3756GH85M78$"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=mIfjOGDM0VQ$"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Sr3m6uA6ETM$"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 04화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDchRGazrwbAJhZELjSRYNyQ2EoXdcQp08thTAAaQipipZgw7CQFPNodzDI3Hip9WpjNtt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=UnbHtYBkUE8$"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=_bYpUfJh-DE$"));
                break;
            case "피키 블라인더스 시즌2" :
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/31/20150502175712750e0ywv3drmv8p0.flv"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20150502175934250tob0yvv0rwzx6.flv"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/41/20150502180148765bchm9zh9y1awn.flv"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/45/20150502180408453wcw5vr1oqv4r8.flv"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/52/2015050218060365640uwys8cg6nrb.flv"));
                listArr.add(new ListViewItem2("피키 블라인더스 시즌2 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/56/20150502180854015r28s7zmjsiwwu.flv"));
                break;
            case "아이스크림 걸스" :
                listArr.add(new ListViewItem2("아이스크림 걸스 - 01화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDchRGazrwbAJhZELjSRYNyZDBfip3sCRhdenCuxCltis5gw7CQFPNodxEyAGGysQBlNt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("아이스크림 걸스 - 02화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDchRGazrwbAJhZELjSRYNyZDBfip3sCRhUqDpNKqJ7eFgw7CQFPNodw6PwfMwjLdW9t2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("아이스크림 걸스 - 03화 완결", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDchRGazrwbAJhZELjSRYNyZDBfip3sCRhZomcbxwxSCngw7CQFPNodzaeAWry7oQnNt2RaEaa4Gr&type=pseudo"));
                break;

            case "빙 휴먼 시즌1" :
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 01화", "http://cmvs.mgoon.com/storage1/m2_video/2009/02/09/2038671.flv?px-bps=1404757&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 02화", "http://cmvs.mgoon.com/storage2/m2_video/2009/03/10/2100546.mp4?px-bps=1405996&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 03화", "http://cmvs.mgoon.com/storage4/m2_video/2009/04/08/2159611.mp4?px-bps=1399219&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2009/08/13/2496450.mp4?px-bps=1353706&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2010/01/07/2834116.mp4?px-bps=1365274&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌1 - 06화 완결", "http://cmvs.mgoon.com/storage2/m2_video/2010/01/17/2862870.mp4?px-bps=1362714&px-bufahead=10"));
                break;
            case "빙 휴먼 시즌2" :
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2010/01/15/2857384.mp4?px-bps=1366065&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2010/01/20/2870727.mp4?px-bps=1366200&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2010/01/28/2895404.mp4?px-bps=1359553&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 04화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/06/2925158.mp4?px-bps=1361778&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 05화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/15/2949581.mp4?px-bps=1360074&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/22/2970670.mp4?px-bps=1369657&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 07화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/22/2970670.mp4?px-bps=1369657&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌2 - 08화 완결", "http://cmvs.mgoon.com/storage2/m2_video/2010/03/02/2994671.mp4?px-bps=1381917&px-bufahead=10"));
                break;
            case "빙 휴먼 시즌3" :
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 01화", "http://cmvs.mgoon.com/storage4/m2_video/2011/01/25/4331713.mp4?px-bps=1395489&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2011/02/02/4341782.mp4?px-bps=1395777&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 03화", "http://cmvs.mgoon.com/storage3/m2_video/2011/02/08/4348518.mp4?px-bps=1395091&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2011/02/15/4359162.mp4?px-bps=1396662&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2011/02/22/4369502.mp4?px-bps=1397832&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2011/03/02/4382441.mp4?px-bps=1400220&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 07화", "http://cmvs.mgoon.com/storage1/m2_video/2011/03/08/4391845.mp4?px-bps=1401972&px-bufahead=10"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌3 - 08화 완결", "http://cmvs.mgoon.com/storage4/m2_video/2011/03/16/4404512.mp4?px-bps=1393591&px-bufahead=10"));
                break;
            case "빙 휴먼 시즌4" :
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 01화", ""));
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 02화", ""));
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 03화", ""));
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 04화", ""));
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 05화", ""));
                listArr.add(new ListViewItem2("빙 휴먼 시즌4 - 06화 완결", ""));
                break;
            case "빙 휴먼 시즌5" :
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/92/20130222125615478bjhq6iht1eoa0.flv"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/46/20130303140338694x3gbv5od96qkh.flv"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/91/20130319162738403z4ck5lk6gjuiv.flv"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/20130321125956210gy2946agl8u3p.flv"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/51/20130324140208613wo5z01uzznd0o.flv"));
                listArr.add(new ListViewItem2("빙 휴먼 시즌5 - 06화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/20130327133629316w2o81zwxpuy0h.flv"));
                break;
            case "미스핏츠 시즌1" :
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/s/s/ssj4475/75/20101213123038453kaiu3b3tusudk.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/g/o/goodie2001/53/20100807184639202wowyb6v88dmpr.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/g/o/goodie2001/35/20101012041658344m2w1j1v2w4ckp.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/s/s/ssj4475/92/20101213123119734rpw9yhg0p301o.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/g/o/goodie2001/54/20100622002133192qwmv3dfqmksl2.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌1 - 06화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/g/o/goodie2001/04/20100731032818993quh690pdit79b.flv"));
                break;
            case "미스핏츠 시즌2" :
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 01화", "http://cmvs.mgoon.com/storage1/m2_video/2010/11/14/4228131.mp4?px-bps=1394205&px-bufahead=10"));
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 02화", ""));
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 03화", ""));
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 04화", ""));
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 05화", ""));
                listArr.add(new ListViewItem2("미스핏츠 시즌2 - 06화 완결", ""));
                break;
            case "미스핏츠 시즌3" :
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/03/20111106224746915kxgex96eu765q.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/63/20111109103626165gqm2nk5ux39hb.flv"));
                //listArr.add(new ListViewItem2("미스핏츠 시즌3 - 03화", ""));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/52/20111124124915649g3gxk62pjui1k.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/98/20111204235645102vs3hssc8668zz.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/52/201112100035117907p1gns1eqy2mf.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/51/20111214124022586ga0ibroh503wj.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌3 - 08화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/g/o/goodie2001/55/20111222104413399p2s8dinsr6leg.flv"));
                break;
            case "미스핏츠 시즌4" :
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20121101144128082vyhp7qiui1xld.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/99/201211071433180337cr69nu3vquzc.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/65/20121118021229193beglar0bwdpq9.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/95/20121122222022312qpxvuc3kqaujg.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/63/20121202201222303cksox5mw9k4ua.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/69/20121205101816334rgr0o74y75vxh.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/88/20121212113008668ct878nyajifv6.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌4 - 08화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/201311030610437304ztsotrf4kz08.flv"));
                break;
            case "미스핏츠 시즌5" :
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/20131029110643456tsijuf3vk678j.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/09/20131107163557105ssmvq4qbwarsm.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/82/20131109163949167aj9t2pzydnj3s.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/78/20131117190026292v9p1a1133ucdf.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/12/20131124173725044l2dw6h7xuqwte.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 06화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNyBMPip4sXgRCOGwDBtsRJjisgw7CQFPNodwCDTripjJIgb9t2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/20/20131209144025099qug4vsy5vyof5.flv"));
                listArr.add(new ListViewItem2("미스핏츠 시즌5 - 08화 완결", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvIa9s3isY47vjJhZELjSRYNy3L7GeDFpGh6GlSzQkDxbtgw7CQFPNodwdX6HNfWhAGNt2RaEaa4Gr&type=pseudo"));
                break;
            case "IT 크라우드 시즌1" :
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2009/07/26/2455864.mp4?px-bps=1371901&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 02화", "http://cmvs.mgoon.com/storage4/m2_video/2009/07/26/2455876.mp4?px-bps=1370524&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2009/07/26/2455896.mp4?px-bps=1365136&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2009/07/26/2455891.mp4?px-bps=1365949&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2009/07/26/2455911.mp4?px-bps=1372660&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌1 - 06화", "http://cmvs.mgoon.com/storage2/m2_video/2009/07/26/2455924.mp4?px-bps=1373101&px-bufahead=10"));
                break;
            case "IT 크라우드 시즌2" :
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 01화", "http://cmvs.mgoon.com/storage4/m2_video/2008/11/08/1782632.flv?px-bps=1409041&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 02화", "http://cmvs.mgoon.com/storage1/m2_video/2008/11/08/1782662.flv?px-bps=1407904&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2008/11/08/1782678.flv?px-bps=1169809&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 04화", "http://cmvs.mgoon.com/storage3/m2_video/2008/11/08/1782866.flv?px-bps=1285725&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 05화", "http://cmvs.mgoon.com/storage1/m2_video/2008/11/08/1783270.flv?px-bps=1249635&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌2 - 06화", "http://cmvs.mgoon.com/storage1/m2_video/2008/11/08/1783285.flv?px-bps=1405531&px-bufahead=10"));
                break;
            case "IT 크라우드 시즌3" :
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 01화", "http://cmvs.mgoon.com/storage3/m2_video/2009/02/08/2035616.mp4?px-bps=1398685&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2009/02/08/2035622.mp4?px-bps=1400733&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 03화", "http://cmvs.mgoon.com/storage1/m2_video/2009/02/08/2035619.flv?px-bps=1408932&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2009/02/08/2035645.mp4?px-bps=1418128&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2009/02/08/2035655.mp4?px-bps=1408216&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌3 - 06화", "http://cmvs.mgoon.com/storage2/m2_video/2009/02/08/2035679.mp4?px-bps=1400155&px-bufahead=10"));
                break;
            case "IT 크라우드 시즌4" :
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 01화", "http://cmvs.mgoon.com/storage4/m2_video/2012/03/04/4944662.mp4?px-bps=1698685&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 02화", "http://cmvs.mgoon.com/storage2/m2_video/2012/03/04/4944663.mp4?px-bps=1700572&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 03화", "http://cmvs.mgoon.com/storage4/m2_video/2012/03/04/4944664.mp4?px-bps=1691943&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 04화", "http://cmvs.mgoon.com/storage4/m2_video/2012/03/04/4944667.mp4?px-bps=1697142&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2012/03/04/4944669.mp4?px-bps=1694985&px-bufahead=10"));
                listArr.add(new ListViewItem2("IT 크라우드 시즌4 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2012/03/04/4944672.mp4?px-bps=1710313&px-bufahead=10"));
                break;
            case "IT 크라우드 라스트 the last byte" :
                listArr.add(new ListViewItem2("IT 크라우드 the last byte", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/68/20150726194957236pvl11bfuzy7h4.flv"));
                break;
            case "빅토리아 시즌1" :
                listArr.add(new ListViewItem2("빅토리아 시즌1 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/a/s/asd8888/17/201609011858070926lwu1fv4qs6gv.flv?key1=38344234353530323831373330313230343530393631364132354534&key2=1C0AD755A1297A3A14E3561CDA0866&ft=FC&class=normal&country=KR&pcode2=45465"));
                listArr.add(new ListViewItem2("빅토리아 시즌1 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/a/s/asd8888/73/2016090618542322950tsr276cxtns.flv?key1=44354337363335303530323230304130373230394231363631354236&key2=EC7FA364D3DF0C4FE0E21558A67FE9&ft=FC&class=normal&country=KR&pcode2=23862"));

                break;
            case "로빈후드 시즌1" :
                listArr.add(new ListViewItem2("로빈후드 시즌1 01화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1493888.flv?px-bps=1449592&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 02화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1493923.flv?px-bps=1440384&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 03화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1493982.flv?px-bps=1437762&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 04화", "http://cmvs.mgoon.com/storage4/m2_video/2008/04/21/1494028.flv?px-bps=1437391&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 05화", "http://cmvs.mgoon.com/storage2/m2_video/2008/04/21/1494047.flv?px-bps=1431501&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 06화", "http://cmvs.mgoon.com/storage2/m2_video/2008/04/21/1494084.flv?px-bps=1440283&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 07화", "http://cmvs.mgoon.com/storage4/m2_video/2008/04/21/1494142.flv?px-bps=1459032&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 08화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1494190.flv?px-bps=1449235&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 09화", "ttp://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1494219.flv?px-bps=1447986&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 10화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/21/1494257.flv?px-bps=1442773&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 11화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/21/1494275.flv?px-bps=1449090&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 12화", "http://cmvs.mgoon.com/storage4/m2_video/2008/04/21/1494336.flv?px-bps=1418991&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌1 13화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/21/1494403.flv?px-bps=1447308&px-bufahead=10"));
                break;
            case "로빈후드 시즌2" :
                listArr.add(new ListViewItem2("로빈후드 시즌2 01화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495283.flv?px-bps=1432417&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 02화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495337.flv?px-bps=1447792&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 03화", "http://cmvs.mgoon.com/storage2/m2_video/2008/04/22/1495378.flv?px-bps=1458384&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 04화", "http://cmvs.mgoon.com/storage2/m2_video/2008/04/22/1495393.flv?px-bps=1454992&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 05화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/22/1495435.flv?px-bps=1458933&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 06화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495457.flv?px-bps=1440987&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 07화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495481.flv?px-bps=1444350&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 08화", "http://cmvs.mgoon.com/storage4/m2_video/2008/04/22/1495514.flv?px-bps=1444023&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 09화", "http://cmvs.mgoon.com/storage4/m2_video/2008/04/22/1495541.flv?px-bps=1441237&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 10화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495552.flv?px-bps=1455964&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 11화", "http://cmvs.mgoon.com/storage1/m2_video/2008/04/22/1495591.flv?px-bps=1437435&px-bufahead=10"));
                listArr.add(new ListViewItem2("로빈후드 시즌2 12화", "http://cmvs.mgoon.com/storage3/m2_video/2008/04/22/1495695.flv?px-bps=1422873&px-bufahead=10"));
                break;
        }


        return listArr;
    }

}
